## Introduction

EUPT (short for „Edition des ugaritischen poetischen Textkorpus“) is a project that aims to create an edition and philological and poetological analysis of the Ugaritic poetic texts.

## The EUPT TextAPI

The EUPT TextAPI is fully compliant to the generic TextAPI and provides some addition as needed. The terminology of the generic TextAPI applies as follows:

    Collection: This refers to all Ugaritic poetic texts.
    Manifest: The stone table on which a text is written.
    Item: The content of the stone table.

## The EUPT AnnotationAPI

To be designed and implemented.

## Purpose of This Documentation

This document aims to describe in which way its TextAPI differs from the generic one and how its AnnotationAPI is designed.
