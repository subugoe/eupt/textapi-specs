---
title: TextAPI Specification
version: 0.0.1
---

## Preliminaries

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [BCP 14](https://tools.ietf.org/html/bcp14),
[RFC2119](https://tools.ietf.org/html/rfc2119) and
[RFC8174](https://tools.ietf.org/html/rfc8174) when, and only when, they appear
in all capitals, as shown here.

This document is licensed under [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode) ([via SPDX](https://spdx.org/licenses/CC-BY-ND-4.0.html)).

The style of this documentation is adapted from [OpenAPI 3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
specification.

## Status of this Document

The current version of this document is: `0.0.1`.

It refers to the following TextAPI version: `1.4.0`

## Definitions

### Cardinalities

| Cardinality | Description | REQUIRED |
|----|----|----|
| 1 | exactly one | yes |
| ? | zero or one | no |

### Data Types

| Type | Description |
|----|----|
| \[…\] | array |
| int | integer |
| string | a sequence of characters, MAY be empty |
| boolean | `true` or `false` |
| URL | a valid URL pointing to a resource |
| MIME type | a valid MIME type according to IANA. See [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types) |
| semver | a string matching the pattern `^\d+\.\d+\.\d+$`, representing a semantic version number |
| iso639-3 | alpha-3 language code according to [ISO 639-3](https://iso639-3.sil.org/) |
| SPDX | a valid SPDX identifier (see <https://spdx.org/licenses/>) or – if not applicable, e.g. for copyrighted resources – `restricted` |

### URI Syntax

The URI Syntax is used according to the one described at [IIIF](https://iiif.io/api/image/2.1/#uri-syntax).

We encourage to allow for IRI (an internationalizes superset or URI) on client side.

## Delivery Service

The TextAPI delivery service is the description of endpoints.
It is REQUIRED to use the `https` protocol.

### Collection

Returns the [Collection Object](#collection-object).

`https://eupt-dev.sub.uni-goettingen.de/api/eupt/collection.json`

<!-- #### Parameters

When a paginated result is given, the total number of items in the sequence MUST be
provided in the response.

| Parameter | Type | Description |
| --- | --- | --- |
| from | int | zero-based number of manifest to include in the sequence object |
| size | int | total of manifests to include in the sequence object |

-->

### Manifest

Returns a [Manifest Object](#manifest-object).

`https://eupt-dev.sub.uni-goettingen.de/api/eupt/KTU_1_14/manifest.json`

<!-- #### Parameters

| Parameter | Type | Description |
| --- | --- | --- |
| from | int | zero-based number of items to include in the sequence object |
| size | int | total of items to include in the sequence object |

When the aforementioned parameters are not set, the default is to deliver the complete list.
-->

### Item

Returns an [Item Object](#item-object).

`https://eupt-dev.sub.uni-goettingen.de/api/eupt/KTU_1_14/1/full.json`

## Schema

All fields that are not explicitly REQUIRED or described with MUST or SHALL
are considered OPTIONAL.
It is RECOMMENDED, however, to provide as much information as possible.

### Actor Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/actor.jsonld` |
| role | 1 | [string] | the role of a personal entity in relation to the parent object, MUST be `collector` in case of collections |
| name | 1 | string | the principal name of the person |

<!--| id | ? | string | internal identifier |
| idref | ? | \[[Idref Object](#idref-object)\] | authority files related to the person | -->

### Collection Object

A collection contains a curated list of texts.
It is REQUIRED to be served at the corresponding [endpoint](#collection).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/collection.jsonld` |
| textapi | 1 | semver | the TextAPI version covered by the implementation |
| id | 1 | URL | URL pointing to this collection |
| title | 1 | \[[Title Object](#title-object)\] | the title of the collection. the main title `MUST` be the first one in the array |
| collector | 1 | \[[Actor Object](#actor-object)\] | a personal entity responsible for the collection (collector) |
| description | ? | string | description of the collection |
| sequence | 1 |\[[Sequence Object](#sequence-object)\] | a set of manifests included in this collection |

<!--| total | ? | int | Number of items the sequence might provide. Required when response is paginated. |-->
<!-- | annotationCollection | ? | URL | URL pointing to an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) for the complete collection | -->

### Content Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/content.jsonld` |
| url | 1 | URL | URL pointing to the content |
| type | 1 | MIME type | a MIME type. either `text/html;type=transcription` or `text/html;type=philology` |

<!-- | integrity | ? | [Data Integrity Object](#data-integrity-object) | information on data integrity | -->

### Idref Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/idref.jsonld` |
| type | 1 | string | short title of the referenced authority |
| id | 1 | string | the main ID referenced |

<!-- | base | ? | URL | the base URL to the authority file | -->

### Item Object

It is REQUIRED to be served at the corresponding [endpoint](#item).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/item.jsonld` |
| textapi | 1 | semver | version number satisfied by the implementation |
| id | 1 | URL | URL pointing to this item |
| type | 1 | string | one of `section`, `page`, `full` |
| lang | 1 | [iso639-3] | language codes describing the resource |
| content | 1 | \[[Content Object](#content-object)\] | different serializations of the item, e.g. HTML, plain text, XML, ... |

<!-- | title | ? | \[[Title Object](#title-object)\] | the title of the item. the main title `MUST` be the first one in the array | -->
<!-- | n | ? | string | division number | -->
<!-- | langAlt | ? | [string] | alternative language name or code (when there is no iso639-3 code, e.g. `karshuni`) | -->
<!-- | description | ? | string | a short description of the object |
| image | ? | [Image Object](#image-object) | corresponding image |
| annotationCollection | ? | URL | URL pointing to an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) for this item | -->

### License Object

The license or any other appropriate rights statement the resources is served under.
It is REQUIRED to use one of [SPDX](https://spdx.org/licenses/).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| id | 1 | SPDX | an SPDX identifier |
<!-- | notes | ? | string | further notes concerning the license. can be used e.g. for the attribution statement of CC BY. `restricted` License Objects `SHOULD` set the `notes` property. It is `RECOMMENDED` to adhere to the [format of copyright notices](https://reuse.software/spec/#format-of-copyright-notices) suggested by the [REUSE Specification](https://reuse.software/spec) | -->

### Manifest Object

This is the main object in the schema to represent a single text, its derivatives
(e.g. html) and therefore containing the metadata.
It is REQUIRED to be served at the corresponding [endpoint](#manifest).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/manifest.jsonld` |
| textapi | 1 | semver | version number satisfied by the implementation |
| id | 1 | URL | URL pointing to this manifest |
| label | 1 | string | human-readable name or title |
| sequence | 1 | \[[Sequence Object](#sequence-object)\] | a sequence of `items` |
| license | 1 | \[[License Object](#license-object)\] | license under which the resource MUST be used |

<!--| total | ? | int | Number of items the sequence might provide. Required when response is paginated. |
| actor | ? | \[[Actor Object](#actor-object)\] | a personal entity related to the document (e.g. author or editor) |
| repository | ? | \[[Repository Object](#repository-object)\] | a repository archiving the document(s) or source |
| image | ? | [Image Object](#image-object) | an image representing the resources (e.g. thumbnail or logo) |
| metadata | ? | \[[Metadata Object](#metadata-object)\] | further metadata |
| support | ? | \[[Support Object](#support-object)\] | -->
<!--| description | ? | string | a short description of the object |
| annotationCollection | ? | URL | URL pointing to an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) for the complete manifest | -->

### Repository Object

A repository archiving the source or derivates, e.g. facsimiles or digitized
versions.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/repository.jsonld` |
| url | 1 | URL | URL pointing to the website of the institution |
| baseUrl | 1 | URL | a base URL where `id` can be resolved |
| id | 1 | string | the identifier at the hosting institution |

<!-- | label | ? | string | the label as given by the hosting institution |-->

### Sequence Object

Represents a sequence of manifests or items.
Within a manifest it SHOULD contain items exclusively.

<!-- A sequence might be separated into chunks on request (GET parameters `from` and `size`). Thus implementations has to maintain the order of items in this array. -->

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/sequence.jsonld` |
| id | 1 | URL | URL to find a [Manifest Object](#manifest-object) or [Item Object](#item-object) |
| type | 1 | string | one of `manifest`, `item` |
| label | 1 | string | human-readable name or title. `SHOULD` correspond to the collection's/manifest's/item's `label` or `title` property |

### Support Object

Any material supporting the view is described and referenced in this object.
This encompasses fonts and CSS.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/support.jsonld` |
| type | 1 | one of `font`, `css` |
| mime | 1 | MIME type | the MIME type for the resource |
| url | 1 | URL | URL pointing to the resource |

<!-- | integrity | ? | [Data Integrity Object](#data-integrity-object) | information on data integrity | -->

### Title Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/title.jsonld` |
| title | 1 | string | a single title |
| type | 1 | string | one of `main`, `sub`. if several titles are provided, at least one of them MUST have the type `main` |

<!-- ### Metadata Object

A set of metadata describing the source or its context.
Mainly used for key-value pairs.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| key | 1 | string | label |
| value | ? | string  | property. MUST be set if the object has no `metadata` field. MUST NOT be set if the object has a `metadata` field. |
| metadata | ? | \[[Metadata Object](#metadata-object)\] | further metadata that is subordinant to the current metadata entry. MUST be set if the object has no `value` field. MUST NOT be set if the object has a `value` field. | -->

<!-- ### Image Object

An image representing the source or its provider.
It is recommended that a [IIIF Image API](https://iiif.io/api/image/2.0/) service is available for this image for manipulations such as resizing.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/textapi/specs/-/raw/main/jsonld/image.jsonld` |
| id | 1 | URL | URL pointing to the image |
| manifest | ? | URL | URL pointing to the image's manifest object |
| license | 1 | [License Object](#license-object) | the license for the image that MUST be used | -->

<!-- ### Data Integrity Object

This object contains information regarding data integrity (checksum) of the referenced resources. It `SHOULD` be used for all resources.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| type | 1 | string | algorithm to calculate checksum (e.g. `MD5`, `SHA-256`) |
| value | 1 | string | the checksum |
-->

## Appendix

### Class Diagram

A class diagram will be added in version 1.0.0.
