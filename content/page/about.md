---
title: About
---


This specification aims to provide insights of how the generic [SUB TextAPI](https://textapi.sub.uni-goettingen.de) can be implemented and enhanced for a specific project. It serves as a basis for the common understanding of front end and back end developers and is – hopefully – of use to others who wish to create API derivates for their own projects.

Technically this documentation is a fork of the generic TextAPI’s repository (although the fork relation has been removed).
