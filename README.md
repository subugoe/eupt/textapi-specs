# TextAPI Documentation

This repo holds the specification for the EUPT specific [TextAPI](https://textapi.sub.uni-goettingen.de).

## Getting the Pages Started Locally

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* [`hugo`](https://gohugo.io/getting-started/installing/)

### Installing

#### Building locally

To work locally with this project, you'll have to follow the steps below:

1. Clone or download this project
2. Switch to the project's directory and type `hugo server` to preview the page
3. Add content
4. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation](https://gohugo.io/overview/introduction/).

##### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from <http://themes.gohugo.io/beautifulhugo/> and changed to work for Hugo > 0.92.0.

## Deployment

This project's static Pages are built by GitLab CI, following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).
The page is built after each commit to the `main` branch.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Mathias Göbel** - *Initial work* - [mgoebel](https://gitlab.gwdg.de/mgoebel)
* **Michelle Weidling** - [mrodzis](https://gitlab.gwdg.de/mrodzis)
* **Tillmann Dönicke** - [tillmann.doenicke](https://gitlab.gwdg.de/tillmann.doenicke)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
